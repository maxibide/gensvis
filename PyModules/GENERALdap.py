# -------------------------------------------------------------------
# - NAME:        GENSdap.py
# - AUTHOR:      Reto Stauffer
# - DATE:        2015-01-15
# -------------------------------------------------------------------
# - DESCRIPTION:
# -------------------------------------------------------------------
# - EDITORIAL:   2015-01-15, RS: Created file on thinkreto.
# -------------------------------------------------------------------
# - L@ST MODIFIED: 2018-05-01 08:48 on marvin
# -------------------------------------------------------------------


import pydap, sys, os
import numpy as np
from datetime import datetime as dt
import re
import utils
# - Setting timezone to UTC, just in case.
os.environ['TZ'] = 'UTC'

class GENERALdap(object):

   def __init__(self,config,showvarlistonly=False):

      print ' * Initializing %sdap object' % self.model.upper()
      self.config = config
      self.box = None

      # - User input set?
      if not self.config['input_userdate']  == None:
         self.latestrun = self.config['input_userdate']
      else:
         self.check_latest_run()

      # - Open Pydap connection
      from datetime import datetime as dt
      YYYYMMDD  = self.latestrun.strftime('%Y%m%d')
      HH        = self.latestrun.strftime('%H')
      file = self.config['dapurl'].replace('YYYYMMDD',YYYYMMDD).replace('HH',HH) 
      print "   - OpenDAP url %s" % file

      try:
         self.dap  = pydap.client.open_url(file)
      except Exception as e:
         import utils
         print "This error occured:"
         print e
         utils.exit('DAP url not ready yet %s' % file) 

      # - This is a development mode. If showvarlistonly is
      #   True just show the keys of the variables and its dimension
      #   names and exit afterwards.
      if showvarlistonly:
         import utils
         for k in self.dap.keys():
            print '   - %s:' % k
            if 'GridType' in str(type(self.dap[k])):
               print '      +- %s' % (', '.join(self.dap[k].keys()))
            else:
               print '      +- dimension array'
         sys.exit("\n\nGENSdap: varlistonly was called. Stop now.\n\n");

      self.lon  = self.dap['lon'].data[:]
      self.lat  = self.dap['lat'].data[:]
      if 'ens' in self.dap.keys():
         self.ens  = self.dap['ens'].data[:]
      else:
         self.ens = None
      # - Matplotlib has one day offset to the format we get
      #   from the OpenDAP server!
      self.time = self.dap['time'].data[:] - 1
      self.init = np.min( self.time )
      if not config['laststep'] == None:
         if config['laststep'] >= len(self.time):
            print "    Last time step too big in config file (%d). Set to %d (maximum)" % \
               (config['laststep'],len(self.time))
            config['laststep'] = len(self.time)

      # - Converting self.init (initial run) to datetime object so that
      #   we can propperly extract the hour
      #   Date in the OpenDAP srever has origin 0000-00-00 (stupid,
      #   however) remove 1970-01-01 to remove the origin offset,
      #   convert in propper unix timestamp and create datetime obj.
      self.initdate = self.__time_to_datetime__(self.init) 
      ##self.initdate = dt.fromtimestamp( (self.init-719164.) * 86400 )

      #import utils

      # - Gridbox size
      self.dy = abs( self.lat[0] - self.lat[1] )
      self.dx = abs( self.lon[0] - self.lon[1] )

      # - Number of time steps we do have to plot!
      self.tmin = None
      self.tmax = config['laststep']
      # - Number of members to plot
      self.mmin = None
      self.mmax = None

      # - Setting index for the time we would like to load.
      if self.tmin == None:  self.tmin = 1
      if self.tmax == None:  self.tmax = len(self.time)-1
      # - If there was an ensemble key in the data (GENS)
      #   we als have to define which members you would like to load.
      if not type(self.ens) == type(None):
         if self.mmin == None:  self.mmin = 0
         if self.mmax == None:  self.mmax = len(self.ens)-1

      # - The +1 is to get propper range.
      #   1:3 is [1,2] but we would like to have
      #   1:(3+1) = [1,2,3] 
      # - Cut the requested times out of the full time vector
      self.time = self.time[self.tmin:self.tmax+1]
      # - Same for the ensemble
      if not type(self.ens) == type(None):
         self.ens  = self.ens[self.mmin:self.mmax+1]

      self.data_container = {} 

      import utils
      statlist = '%s/%s' % (self.config['statlistdir'],self.config['statlist'])
      self.stations = utils.read_statlist(statlist)


   # ----------------------------------------------------------------
   # - A helper function removing html code from a line.
   # ----------------------------------------------------------------
   def __cleanhtml__(self,raw_html):
      cleanr =re.compile('<.*?>')
      cleantext = re.sub(cleanr,'', raw_html).strip()
      return cleantext


   # - Converting time to datetime object in a propper way.
   def __time_to_datetime__(self,x):
      # - Converting self.init (initial run) to datetime object so that
      #   we can propperly extract the hour
      #   Date in the OpenDAP srever has origin 0000-00-00 (stupid,
      #   however) remove 1970-01-01 to remove the origin offset,
      #   convert in propper unix timestamp and create datetime obj.
      return dt.fromtimestamp( (x-719163.) * 86400 )

   # ----------------------------------------------------------------
   # - Small helper function to extract the necessry information
   #   from a 'html line'. 
   # ----------------------------------------------------------------
   def __search_latest_extract_info__(self,url,reg):

      from urllib import urlopen

      res = []
      inv = urlopen( url ) 
      inv = inv.readlines()
      for line in inv:
         line = self.__cleanhtml__(line)
         line = re.findall(r'%s' % reg,line)
         if len(line) == 0: continue
         # - Extracting date (we would like to find the newest one)
         res.append(line[0]) 

      return res 


   # ----------------------------------------------------------------
   # - Checking the latest run which is online.
   # ----------------------------------------------------------------
   def check_latest_run(self):

      if not self.config['input_userdate'] == None:
         print "   Do NOT check latest run: manual date/runhour choosen!"
         return

      import utils

      # - Searching for newest directory 
      url = '/'.join( self.config['dapurl'].split('/')[0:-2] )
      dirs = self.__search_latest_extract_info__(url,'%s[0-9]{8}' % self.model.lower() )
      date_dir = None
      for d in dirs:
         d = dt.strptime(d.replace(self.model.lower(),''),'%Y%m%d')
         if date_dir == None: date_dir = d
         if d > date_dir: date_dir = d

      if date_dir == None:
         utils.exit('Could not find any sutable directory on the nomads opendap direcotry')
      else:
         print "   - Latest GENS OpenDAP directory: %s" % date_dir.strftime('%Y-%m-%d')

      # - Searching for newest run
      file_string = self.config['dapurl'].split('/')[-1].replace('HH','[0-9]{2}')
      files = self.__search_latest_extract_info__('%s/%s%s' % \
            (url,self.model.lower(),date_dir.strftime('%Y%m%d')),file_string)
      runhour = None
      for f in files:
         try:
            ##f = int(f.replace('gep_all_','').replace('z',''))
            ##f = self.__get_runhour_from_file__(f)
            f = int( f.split('_')[-1].replace('z','') )
         except:
            continue
         if runhour == None: runhour = f
         if f > runhour: runhour = f

      # - No file found
      if runhour == None:
         utils.exit('Cannot find sutable dap file (runhour extraction returned None)')

      self.latestrun = dt.strptime('%s %02d' % (date_dir.strftime('%Y%m%d'),runhour),'%Y%m%d %H')
      print "   - Latest %s OpenDAP run is then: %s" % \
            (self.model,self.latestrun.strftime('%Y-%m-%d %H:00 UTC'))

      # - Full path to the checkfile
      checkfile = "%s/%s" % (self.config['img_dir'],self.config['checkfile'])

      # - Check if allready run
      print '     Checking the %s checkfile - allready visualized this run?' % checkfile
      cfcontent = None
      if os.path.isfile( checkfile ):
         cfcontent = open(checkfile,'r').readline().replace('\n','').strip()
         try:
            cfcontent = dt.strptime(cfcontent,'%Y-%m-%d %H:%M')
         except:
            print "   - Content of %s not in propper format! Ignore." % checkfile
            cfcontent = None
      else:
         print "   - No such file - expect that run not downloaded at the moment"

      if not cfcontent == None:
         if cfcontent >= self.latestrun:
            sys.exit('Allready visualized this run, we can quit now ...')


   # ----------------------------------------------------------------
   # - Create checkfile. 
   # ----------------------------------------------------------------
   def create_checkfile(self):

      # - If user has had input date do not write lastrun   
      #   file. Only if the script was started in the automatic
      #   mode.
      if not self.config['input_userdate'] == None:
         return

      # - Touch the file
      try:
         fid = open("%s/%s" % (self.config['img_dir'],self.config['checkfile']),'w')
         fid.write("%s" % self.latestrun.strftime('%Y-%m-%d %H:%M'))
         fid.flush()
         fid.close()
      except Exception as inst:
         print inst
         print "!!!!!!!!!!! problems writing the checkfile into image directory [image][dir]"

      # - Do the same for the 'datadir'
      if not self.config['datadir'] == None:
         try:
            fid = open("%s/%s" % (self.config['datadir'],self.config['checkfile']),'w')
            fid.write("%s" % self.latestrun.strftime('%Y-%m-%d %H:%M'))
            fid.flush()
            fid.close()
         except Exception as inst:
            print inst
            print "!!!!!!!!!!! problems writing the checkfile to the datadir"


   # ----------------------------------------------------------------
   # - Find closest points
   #   Append the information to the station list and also define
   #   two arrays indI, and indJ which contain all the indize.
   #   They will be used to select the bounding box later on.
   # ----------------------------------------------------------------
   def nearest_neighbors(self):

      indI = []; indJ = []
      for s in range(0,len(self.stations)):
         i,j = utils.nearest(self,s)
         self.stations[s]['nn_lat'] = i
         self.stations[s]['nn_lon'] = j
         for ii in i: indI.append(ii)
         for jj in j: indJ.append(jj)

      self.indI = indI
      self.indJ = indJ

      # - Setting bounding box
      self.setboundingbox(indI,indJ)

      return indI, indJ


   ## ----------------------------------------------------------------
   ## - The pydap opendap object can be called by keys. 
   ##   Loadng datas with bounding box self.box - if set.
   ## ----------------------------------------------------------------
   #def getdata(self,key):

   #   # - Box has to have length 4
   #   if not len(self.box) == 4:
   #      sys.exit('ERROR: in GNSdap.getdata, box has to have 4 elements!')
   #   minI = self.box[0]
   #   maxI = self.box[1]
   #   minJ = self.box[2]
   #   maxJ = self.box[3]

   #   # -baaa
   #   if not key in self.dap.keys():
   #      sys.exit('Sorry, key %s not in dap object. Stop in getdata.' % key)

   #   # - If I/J is just the same: loading one point.
   #   print '    - Loading data for \"%s\": [%d:%d,%d:%d,%d:%d,%d:%d]' % \
   #      (key,self.mmin,self.mmax,self.tmin,self.tmax,minI,maxI,minJ,maxJ)
   #   print '      OpenDAP object dimension for %s: %s' % (key,str(self.dap[key].shape))
   #   # - The +1 is important to extend the range!
   #   #   1:3 is [1,2]. Therefore we have to load
   #   #   1:(3+1) to get [1,2,3]
   #   data = self.dap[key].data[0][self.mmin:(self.mmax+1),self.tmin:(self.tmax+1),minI:(maxI+1),minJ:(maxJ+1)]
   #   print '   - Data loaded, data size: %s' % str(data.shape)

   #   return {'param':key,'data':data}

   # ----------------------------------------------------------------
   # - The bounding box is the area where we have to load the data for.
   #   boundingbox is a list containing INDIZES for longitude and
   #   latitude. Latitude on I, longitude on J corresponding to 
   #   the data matrix itself.
   # ----------------------------------------------------------------
   def setboundingbox(self,indI,indJ):

      minI = np.min(indI)
      maxI = np.max(indI)
      minJ = np.min(indJ)
      maxJ = np.max(indJ)

      self.box = [minI,maxI,minJ,maxJ]



   # ---------------------------------------------------------------- 
   # - Maipulating some of the data
   # ---------------------------------------------------------------- 
   def dataprocessing(self):

      import numpy as np

      # - First step: Loop over all variables, searching for
      #   offset and factor definition and scale the data if necessay.
      for key in self.data_container.keys():
         # - Searching for parameter configuration here
         #   Setting defaults and overwrite if possible.
         offset  = None
         factor  = None
         if 'parameter_%s' % key in self.config.keys():
            settings = self.config['parameter_%s' % key]
            if 'offset' in settings.keys(): offset = settings['offset']
            if 'factor' in settings.keys(): factor = settings['factor']
         # - If offset or factor (or both) set:
         if not offset == None:
            self.data_container[key] = self.data_container[key] - offset
         if not factor == None:
            self.data_container[key] = self.data_container[key] * factor

      # - Then searching for postprocessig variables.
      #   They are hardcoded here
      if 'ugrd10m' in self.data_container.keys() and \
         'vgrd10m' in self.data_container.keys():

         ff = np.zeros( self.data_container['ugrd10m'].shape, dtype='float' )
         dd = np.zeros( self.data_container['ugrd10m'].shape, dtype='float' )
         for i in range(0,len(self.stations)):
            u = self.data_container['ugrd10m'][i,]
            v = self.data_container['vgrd10m'][i,]
            ff[i,] = np.sqrt( u**2 + v**2 )
            dd[i,] = self.__uv2ddff__(u,v)
         self.data_container['ff10m'] = ff 
         self.data_container['dd10m'] = dd

         del self.data_container['ugrd10m']
         del self.data_container['vgrd10m']

      # - Then searching for postprocessig variables.
      #   They are hardcoded here
      if 'ugrd100m' in self.data_container.keys() and \
         'vgrd100m' in self.data_container.keys():

         ff = np.zeros( self.data_container['ugrd100m'].shape, dtype='float' )
         dd = np.zeros( self.data_container['ugrd100m'].shape, dtype='float' )
         for i in range(0,len(self.stations)):
            u = self.data_container['ugrd100m'][i,]
            v = self.data_container['vgrd100m'][i,]
            ff[i,] = np.sqrt( u**2 + v**2 )
            dd[i,] = self.__uv2ddff__(u,v)
         self.data_container['ff100m'] = ff 
         self.data_container['dd100m'] = dd

         del self.data_container['ugrd100m']
         del self.data_container['vgrd100m']



   # ---------------------------------------------------------------- 
   # - Compute u to v
   # ---------------------------------------------------------------- 
   def __uv2ddff__(self,u,v):

      import numpy as np

      if len(u) == 0 or len(v) == 0: return np.ndarray((),dtype='float') 

      # - Polar coordiantes
      dd = np.arctan2( u, v) * 180 / np.pi
      dd = dd - 180.

      dd[np.where( dd == 360.)] = 0.
      dd[np.where( dd  <   0.)] = dd[np.where( dd  <   0.)] + 360.

      #### - Polar coordiantes
      ###dd = np.arctan2( u, v) * 180 / np.pi  
      ###dd = dd - 180.
      ###for i in range(0,len(dd)):
      ###   print dd[i]
      ###   if dd[i] == 360.:
      ###      dd[i] = 0.
      ###   elif dd[i] < 0:
      ###      dd[i] = dd[i] + 360.

      return dd


   # ---------------------------------------------------------------- 
   # - Plotting station number (i). Returns False if the station
   #   index is bigger than the stations loaded!
   # ---------------------------------------------------------------- 
   def plot(self,s,typ = 'eps'):

      if not typ in ['eps','leps','det']:
         utils.exit('Sorry, wrong \"typ\" input to plot method. Allowed: eps, leps') 

      if s >= len(self.stations):
         print "   WARNING: station index %d for GENSdl.plot is bigger " + \
               "than the stations loaded. Return False." % (s)
         return False

      # - No stations in the data containre? Well, that excalated
      #   quickly. Stop here.
      if len(self.data_container) == 0:
         utils.exit('Sorry, no data in the GENSdl.data_containre. Cannot plot anything. Stop.')

      # - Some console information
      station = self.stations[s]
      print ' * Visualize ECEPS Meteogram for stations %d' % s
      print '   %-20s %s' % ('Station name:',station['name'])
      print '   %-20s %d' % ('Station number:',station['stnr'])
      print '   %-20s %6.2f' % ('Station longitude:',station['lon'])
      print '   %-20s %6.2f' % ('Station latitude:',station['lat'])
      print '   %-20s %d' % ('Neighbor points:',len(station['nn_lat']))


      # - The list in self.data_container is unsorted. Or at least
      #   somehow randomly sorted. There is a config option to
      #   'wish' an order. If self.config['paramorder'] is set
      #   we try to fulfill the order.
      #   - if paramorder key NOT in data_container keys: skip
      #   - if paramorder key IN     data_containre keys: append
      #   - all data_container keys NOT in paramorder: append to end.
      if not self.config['paramorder'] == None:
         plotorder = []
         # - Take the ones from the sort order first
         for po in self.config['paramorder']:
            if po in self.data_container.keys():  plotorder.append(po)
         # - Now take the 'forgotten' ones
         for key in self.data_container.keys():
            if not key in plotorder:              plotorder.append(key)
      else:
         print '   - No paramordre specified in config file.'
         plotorder = self.data_container.keys()
      print '   - Plotorder:'
      for i in range(0,len(plotorder)): 
         print '    +%2d %s' % ( (i+1), plotorder[i] )

   
      # -------------------------------------------------------------
      # - Create figure now
      # -------------------------------------------------------------
      self.__do_plot__(s,plotorder,typ)


         


