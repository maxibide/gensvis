# -------------------------------------------------------------------
# - NAME:        readconfig.py
# - AUTHOR:      Reto Stauffer
# - DATE:        2015-01-16
# -------------------------------------------------------------------
# - DESCRIPTION:
# -------------------------------------------------------------------
# - EDITORIAL:   2015-01-16, RS: Created file on thinkreto.
# -------------------------------------------------------------------
# - L@ST MODIFIED: 2015-01-21 12:49 on pc24-c707
# -------------------------------------------------------------------




def readconfig(file,inputs,verbose=False):
   
   import sys, os
   import utils

   print " * Reading config file %s now" % file

   if not os.path.isfile(file):
      utils.exit('Sorry, cannot find file %s' % file)

   from ConfigParser import ConfigParser

   CFG = ConfigParser()
   CFG.read(file)

   config = {}

   # ----------------------------------------------------------------
   # - Appending inputs
   # ----------------------------------------------------------------
   for key in inputs.keys():
      config['input_%s' % key] = inputs[key]

   # ----------------------------------------------------------------
   # - Getting variables
   # ----------------------------------------------------------------
   try:
      config['statlist']     = CFG.get('settings','statlist')
      config['statlistdir']  = CFG.get('settings','statlistdir')
      config['checkfile']    = CFG.get('settings','checkfile')
      config['dapurl']       = CFG.get('settings','dapurl')
      config['parameter']    = CFG.get('settings','parameter')
      config['parallel']     = CFG.getint('settings','parallel')
      config['caching']      = CFG.getboolean('settings','caching')
   except Exception as inst:
      print inst
      utils.exit('In config file: section [settings] not propperly defined') 
   try:
      tmp                    = CFG.get('settings','paramorder')
      config['paramorder'] = []
      for elem in tmp.split(','): config['paramorder'].append( elem.strip() )
   except:
      config['paramorder']   = None

   # - Data directory
   try:
      config['datadir']     = CFG.get('settings','datadir')
   except:
      config['datadir']     = None
   if config['datadir'] == None:
      print "   Dont store ASCII data of the forecasts"
   else:
      if not os.path.isdir( config['datadir'] ):
         config['datadir'] = None
         print "   [!] Could not find directory %s" % config['datadir']
         print "   Write out ASCII data disabled automatically!"

   # - Special behaviour: if input -s/--statlist hash was set there is
   #   an expected statlist file on inputs['statlist'] ( != None).
   #   If this is the case, check if input['statlist'] file exists
   #   in the config['statlistdir'] directory. If not, exit here.
   if not inputs['statlist'] == None:
      if not os.path.isfile('%s/%s_statlist.csv' % (config['statlistdir'],inputs['statlist'])):
         utils.exit('Input -s/--statlist was set expecting file %s/%s_statlist.csv but does not exist!' % \
               (config['statlistdir'],inputs['statlist']))
      else:
         config['statlist'] = "%s_statlist.csv" % inputs['statlist']
         # - Also change the checkfile. One checkfile for
         #   each manual station - so that we can cronjob different
         #   areas around the world.
         config['checkfile'] = '%s_%s' % (inputs['statlist'],config['checkfile'])

   # - Checking some necessary paths
   if not os.path.isdir( config['statlistdir'] ):
      utils.exit('Config error: statlistdir %s not found' % config['statlistdir'])
   if not os.path.isdir( config['statlistdir'] ):
      utils.exit('Config error: imgdir %s not found' % config['imgdir'])
   # - Parallel
   if config['parallel'] < 0 or config['parallel'] > 10:
      utils.exit('Config error: parallel has to be between 1 and 10')

   # - Parsind parameter list
   tmp = config['parameter'].strip().split(',')
   config['parameter'] = []
   for elem in tmp: config['parameter'].append( elem.strip() )
   if len(config['parameter']) == 0:
      utils.exit('Tried to parse parameter list from config file - seems to be empty!')


   # ----------------------------------------------------------------
   # - Image settings 
   # ----------------------------------------------------------------
   try:
      config['laststep']         = CFG.getint('image','laststep')
   except:
      config['laststep']         = None
   try:
      config['img_width']        = CFG.getfloat('image','width')
      config['img_height']       = CFG.getfloat('image','height')
      config['img_dir']          = CFG.get('image','dir')
      config['img_postfix']      = CFG.get('image','postfix')
      # - Some font size configs
      config['img_axes_titlesize'] = CFG.get('image','axes_titlesize')
      config['img_xtick_fontsize'] = CFG.get('image','xtick_fontsize')
      config['img_ytick_fontsize'] = CFG.get('image','ytick_fontsize')
      #config['img_']     = CFG.get('image','')
   except Exception as e:
      print e
      utils.exit('Config error in [image] section.')
   # - Checking image destination directory
   if not os.path.isdir( config['img_dir'] ):
      utils.exit('Config error: img_dir %s not found' % config['img_dir'])
   # - Laststep wrong
   if not config['laststep'] == None:
      if config['laststep'] < 1:
         utils.exit('Config error: image laststep has to be bigger equal 1!')

   # ----------------------------------------------------------------
   # - Loading parameter dependent stuff 
   # ----------------------------------------------------------------
   sections = CFG.sections()
   for sec in sections:

      # - We are searching for parameter sections. If the section
      #   name does NOT contain 'parameter ' (with blank) - skip.
      if not 'parameter ' in sec: continue

      # - Else the second part is the parameter name.
      param = sec.strip().split(' ')[1]
      items = CFG.items('parameter %s' % param)

      # - Empty section? Skip.
      if len(items) == 0: continue

      # -------------------------------------------------------------
      # - Helper function
      # -------------------------------------------------------------
      def getval(items,key,tp='str'):

         if not tp in ['str','int','float']: utils.exit('Wrong usage of getval helper function')

         # - Find key
         found = False
         for rec in items:
            k = rec[0]; v = rec[1]
            if k == key:
               found = True; break
         if not found: return None
          
         try:
            if tp == 'str':   val = str(v)
            if tp == 'int':   val = int(v)
            if tp == 'float': val = float(v)
         except Exception as e:
            print e
            utils.exit('Problems parsing config section [parameter %s] value %s' % (param,'yticks'))

         return val


      # -------------------------------------------------------------
      # - If there are elements evaluate them and append
      #   the object to the config dict.
      # -------------------------------------------------------------
      res = {}
      if not getval(items,'title','str')    == None: res['title']  = getval(items,'title','str')
      if not getval(items,'yticks','int')   == None: res['yticks'] = getval(items,'yticks','int')
      if not getval(items,'offset','float') == None: res['offset'] = getval(items,'offset','float')
      if not getval(items,'factor','float') == None: res['factor'] = getval(items,'factor','float')
      if not getval(items,'ymax','float')   == None: res['ymax']   = getval(items,'ymax','float')
      if not getval(items,'ymin','float')   == None: res['ymin']   = getval(items,'ymin','float')

      config['parameter_%s' % param] = res
   



   return config
